﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HarpString : MonoBehaviour
{
    public Note note;

    private Renderer render;
    private AudioSource audioSource;
    private Collider collider;
    private static readonly int Color = Shader.PropertyToID("_Color");
    private Color originalColor;
    private bool isPlaying;
    private void Start ()
    {
        render = GetComponent<Renderer>();
        audioSource = GetComponent<AudioSource>();
        collider = GetComponent<Collider>();
        
        if(render == null)
            Debug.LogWarning("A renderer was not assigned in object " + name + " of type " + GetType());
        else
            originalColor = render.material.color;
        if(audioSource == null)
            Debug.LogWarning("An audiosource was not assigned in object " + name + " of type " + GetType() + 
                               ". Some notes might not get played.");
        if (collider == null)
            Debug.LogWarning("A collider was not assigned in object " + name + " of type " + GetType() +
                             ". Some notes might not get played.");
    }
    
    public void TryPlay(Vector3 position)
    {
        if(collider == null)
            return;
        
        if(collider.bounds.Contains(position))
            Play();
    }
    
    private void Play()
    {
        if(isPlaying)
            return;
        if(render != null)
            render.material.SetColor(Color, note.color);
        
        if(audioSource != null)
            audioSource.PlayOneShot(note.sound);

        StartCoroutine(ChangeColorWhenSoundsEnd());
    }

    private IEnumerator ChangeColorWhenSoundsEnd()
    {
        float timeLeft = note.sound.length;
        while (timeLeft > 0)
        {
            timeLeft -= Time.deltaTime;
            yield return null;
        }

        render.material.color = originalColor;
    }
    
    //TODO : feebbacl lorsqu'on hover le collider

}
