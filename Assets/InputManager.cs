﻿using System.Collections;
using System.Collections.Generic;
using Leap.Unity;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;


[System.Serializable]
public class Vector3Event : UnityEvent<Vector3>{}
/// <summary>
/// Singleton to manage signals from various LeapMotion related scripts
/// All classes that require user input should subscribe to the corresponding UnityEvent
/// </summary>
/// <see cref="PinchDetector"/>
public class InputManager : MonoBehaviour
{
    public PinchDetector rightHand;
    public PinchDetector leftHand;
    
    /// <summary>
    /// Take functions taking a single Vector3 parameter. All these functions
    /// will be called when onAnyPinch.Invoke() is called.
    /// </summary>
    /// <see cref="OnAnyPinch"/>
    public Vector3Event onAnyPinch;
    
    //This object is a singleton
    public static InputManager Instance;

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        if (Instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        if(rightHand == null)
            Debug.LogWarning( name + " : rightHand was not set. Right hand interaction will not be recorded" );
        else
            rightHand.OnActivate.AddListener(delegate { OnAnyPinch(rightHand.Position); });
        
        if(leftHand == null)
            Debug.LogWarning(name + " : leftHand was not set. Left hand interaction will not be recorded");
        else
            leftHand.OnActivate.AddListener(delegate { OnAnyPinch(leftHand.Position); });
    }

    public void OnAnyPinch(Vector3 position)
    {
        onAnyPinch.Invoke(position);
    }


}
