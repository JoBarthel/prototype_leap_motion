# LYRE 

This application uses a Leap motion to detect the hand movement of the user and allow them to play a virtual pinched string instrument. 

# How to install

This app requires a Leap Motion to use.

- Install Unity3D 2018.1 .

- Install the Leap Motion Core Version 4.4.0

- Clone this repo and import it into Unity3D.

# How to use

By performing a pinching motion and pulling, the user can imprint tension to a string. 
When the string is released, it makes a sound. The pitch depends on which string was pinched and the volume on how far the string was pulled. 